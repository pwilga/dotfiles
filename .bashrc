# B:pwilga dotfiles
for file in ~/.{path,bash_prompt,exports,aliases,functions,extra,dockerfunc}; do
	[ -r "$file" ] && [ -f "$file" ] && source "$file";
done;
unset file;

# disable flow control for that terminal completely (not just for vim). It is necessary to allow entering shortcust like Ctrl + s in vim
stty -ixon
# E:pwilga dotfiles
