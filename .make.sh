#!/bin/bash

excluded_files=(. .. .git .bashrc $0)

basedir=$(dirname $(realpath $0))
files=$(ls -a $basedir)

for file in $files; do
    found=$(echo ${excluded_files[*]} | grep "$file")
    if [ "${found}" == "" ]; then
        if [ -h "$HOME/$file" ]; then
           rm -f ~/$file
           echo "Removed softlink: $HOME/$file"
        elif [ -f ~/$file ]; then 
           tmpdir=$(dirname $(mktemp -u))
           mv ~/$file "$tmpdir/backup_$file"
           echo "File backup created: $tmpdir/backup_$file"
        fi
        ln -s $basedir/$file ~/$file
        echo "New link created: $HOME/$file"
    fi
done

if [ -f ~/.bashrc ] ; then 
    if grep "# B:pwilga dotfiles" ~/.bashrc 1>/dev/null ; then
        echo "Appropriate code exists in ~/.bashrc file"
        exit
    fi
fi
cat $basedir/.bashrc >> ~/.bashrc
echo "~/.bashrc updated or created"
